
This is a codebase for Dr. Peters Advanced Computer Graphics (CAP6701) course for spring 2017.  It is based on the "Picking with an OpenGL hack" tutorial found here http://www.opengl-tutorial.org/miscellaneous/clicking-on-objects/picking-with-an-opengl-hack/ but the code has been reorganized to make it easier to work with.

To compile on a Unix system, cd into the "build" directory and type "cmake .."  Once cmake finishes, type "make" and it will compile your code.  It will take a few minutes.  Your executable should now be in the "misc05_picking" folder and will be called "misc05_picking".  This was tested on Ubuntu 16.04 and worked correctly.

If you are on another system, refer to http://www.opengl-tutorial.org/beginners-tutorials/tutorial-1-opening-a-window/ for advice on how to compile.  On Windows, I believe all you need to do is run cmake in the correct directories and it'll generate a Visual Studio .sln file, but I have not tested it myself.

Don't use a Mac.  It's hard to get the code to work due to the retina screen having a non-standard resolution.
